angular.module('ngSoloists.filter', [
	'ngSoloists.CuisineService'
	])

.directive("solFilter", function(CuisineService) {
	return {
		scope: {
			expandOrCollapse:"=",
			filterOptions:"=",
			filterUpdated:"="
		},
		restrict: "A",
		templateUrl: 'directives/filter.tpl.html',
		link: function(scope, element, attrs) {

			scope.filterOptions = {
				crossfit:null,
				mountainbiking:null,
				movies:null,
				photography:null,
				soccer:null,
				trailrunning:null
			};

			scope.filterUpdated = false;

			scope.changingFilter = function(filterVal) {
				scope.filterUpdated = !scope.filterUpdated;
				console.log(scope.filterOptions);
			};

			scope.$watch('filterOptions.soloistsEateries', function() {
				scope.filterOptions.travelersLocals = null;
				scope.filterOptions.businessLeisure = null;
			});

			scope.$watch('filterOptions.travelersLocals', function() {
				scope.filterOptions.businessLeisure = null;
			});



			scope.getCuisineServiceCall = {
				success: function(data, status, headers, config) {
					scope.dataCuisines = data;
				},
				error: function(data, status, headers, config) {
					console.log(status);
				}
			};

			CuisineService.getCuisines(scope.getCuisineServiceCall.success, scope.getCuisineServiceCall.error);

			scope.status = {
				isopen: false
			};

			scope.toggleDropdown = function($event) {
				$event.preventDefault();
				$event.stopPropagation();
				scope.status.isopen = !scope.status.isopen;
			};




			scope.expandFilter = function() {
				$('.directive-filter').removeClass('collapsed');
				$('.directive-profile').removeClass('collapsed');
				$('.content-container').removeClass('collapsed col-md-12').addClass('col-md-9');

				scope.expandOrCollapse = "expanded";
				scope.filterUpdated = !scope.filterUpdated;
			};

		}
	};
})




.filter('propsFilter', function() {
	return function(items, props) {
		var out = [];

		if (angular.isArray(items)) {
			items.forEach(function(item) {
				var itemMatches = false;

				var keys = Object.keys(props);
				for (var i = 0; i < keys.length; i++) {
					var prop = keys[i];
					var text = props[prop].toLowerCase();
					if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
						itemMatches = true;
						break;
					}
				}

				if (itemMatches) {
					out.push(item);
				}
			});
		} else {
      // Let the output be the input untouched
      out = items;
  }

  return out;
};
});