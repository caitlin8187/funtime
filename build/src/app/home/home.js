angular.module( 'ngSoloists.home', [])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home',
    views: {
      "main": {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

.controller( 'HomeCtrl', function HomeController($scope, UserService, YelpService) {

  $scope.expandOrCollapse = "expanded";

  $scope.filterOptions = null;

  // What order is this stream in initially? If by 
  // when the user added themself, how do we ensure 
  // restaurants stay scattered throughout?

  $scope.userServiceCall = {
    success: function(data, status, headers, config) {
      console.log(data);
    },
    error: function(data, status, headers, config) {
      // console.log(status);
    }
  };

  UserService.getUsers($scope.userServiceCall.success, $scope.userServiceCall.error);


  

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  




  $scope.shuffle = function(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  };


  

  $scope.restaurantServiceCall = {
    success: function(data, status, headers, config) {
      $scope.streamData = $scope.shuffle($scope.userData.concat(data.businesses));
    },
    error: function(data, status, headers, config) {
      // console.log(status);
    }
  };


  $scope.restaurantQueryData = {
    cuisine:"Italian",
    zip:"89109",
    page:0,
    perPage:4
  };

  // YelpService.getRestaurants($scope.restaurantServiceCall.success, $scope.restaurantServiceCall.error, $scope.restaurantQueryData);







  $scope.userData = [
    {
      id:0,
      eventTitle:"Bike Banditz",
      timestamp:1431162000,
      eventDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor pretium ligulain facilisis lacus fringilla elementum. Cras sit amet luctus urna, lobortis convallis lectus. Nullam augue ipsum, suscipit ac gravida ac, pellentesque eu justo. Etiam tempor.",
      eventImage:"http://photos1.meetupstatic.com/photos/event/a/d/2/6/600_435164326.jpeg",
      attendees:[
        {
          id:0,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:1,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        },{
          id:2,
          firstName:"Denise",
          lastName:"Rose",
          email:"denise.rose54@example.com",
          phone:"(296)-436-6580",
          location: {
            city:"Bueblo",
            state:"Maine",
            street:"3994 Spring St",
            zip:"49751"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/57.jpg",
            "http://api.randomuser.me/portraits/med/women/57.jpg",
            "http://api.randomuser.me/portraits/thumb/women/57.jpg"
          ]
        },{
          id:3,
          firstName:"Dwight",
          lastName:"Wood",
          email:"dwight.wood38@example.com",
          phone:"(890)-778-9023",
          location: {
            city:"North Valley",
            state:"Ohio",
            street:"3677 Walnut Hill lLn",
            zip:"12967"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/82.jpg",
            "http://api.randomuser.me/portraits/med/men/82.jpg",
            "http://api.randomuser.me/portraits/thumb/men/82.jpg"
          ]
        },{
          id:4,
          firstName:"Roland",
          lastName:"Thomas",
          email:"roland.thomas46@example.com",
          phone:"(711)-326-9419",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"6735 N Stelling Rd",
            zip:"91200"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/38.jpg",
            "http://api.randomuser.me/portraits/med/men/38.jpg",
            "http://api.randomuser.me/portraits/thumb/men/38.jpg"
          ]
        }, {
          id:5,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:6,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        },{
          id:7,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:8,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        },{
          id:9,
          firstName:"Denise",
          lastName:"Rose",
          email:"denise.rose54@example.com",
          phone:"(296)-436-6580",
          location: {
            city:"Bueblo",
            state:"Maine",
            street:"3994 Spring St",
            zip:"49751"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/57.jpg",
            "http://api.randomuser.me/portraits/med/women/57.jpg",
            "http://api.randomuser.me/portraits/thumb/women/57.jpg"
          ]
        },{
          id:10,
          firstName:"Dwight",
          lastName:"Wood",
          email:"dwight.wood38@example.com",
          phone:"(890)-778-9023",
          location: {
            city:"North Valley",
            state:"Ohio",
            street:"3677 Walnut Hill lLn",
            zip:"12967"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/82.jpg",
            "http://api.randomuser.me/portraits/med/men/82.jpg",
            "http://api.randomuser.me/portraits/thumb/men/82.jpg"
          ]
        },{
          id:11,
          firstName:"Roland",
          lastName:"Thomas",
          email:"roland.thomas46@example.com",
          phone:"(711)-326-9419",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"6735 N Stelling Rd",
            zip:"91200"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/38.jpg",
            "http://api.randomuser.me/portraits/med/men/38.jpg",
            "http://api.randomuser.me/portraits/thumb/men/38.jpg"
          ]
        }, {
          id:12,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:13,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        },{
          id:14,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:15,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        },{
          id:16,
          firstName:"Denise",
          lastName:"Rose",
          email:"denise.rose54@example.com",
          phone:"(296)-436-6580",
          location: {
            city:"Bueblo",
            state:"Maine",
            street:"3994 Spring St",
            zip:"49751"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/57.jpg",
            "http://api.randomuser.me/portraits/med/women/57.jpg",
            "http://api.randomuser.me/portraits/thumb/women/57.jpg"
          ]
        },{
          id:17,
          firstName:"Dwight",
          lastName:"Wood",
          email:"dwight.wood38@example.com",
          phone:"(890)-778-9023",
          location: {
            city:"North Valley",
            state:"Ohio",
            street:"3677 Walnut Hill lLn",
            zip:"12967"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/82.jpg",
            "http://api.randomuser.me/portraits/med/men/82.jpg",
            "http://api.randomuser.me/portraits/thumb/men/82.jpg"
          ]
        },{
          id:18,
          firstName:"Roland",
          lastName:"Thomas",
          email:"roland.thomas46@example.com",
          phone:"(711)-326-9419",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"6735 N Stelling Rd",
            zip:"91200"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/38.jpg",
            "http://api.randomuser.me/portraits/med/men/38.jpg",
            "http://api.randomuser.me/portraits/thumb/men/38.jpg"
          ]
        }, {
          id:19,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:20,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        },{
          id:21,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:22,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        },{
          id:23,
          firstName:"Denise",
          lastName:"Rose",
          email:"denise.rose54@example.com",
          phone:"(296)-436-6580",
          location: {
            city:"Bueblo",
            state:"Maine",
            street:"3994 Spring St",
            zip:"49751"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/57.jpg",
            "http://api.randomuser.me/portraits/med/women/57.jpg",
            "http://api.randomuser.me/portraits/thumb/women/57.jpg"
          ]
        },{
          id:24,
          firstName:"Dwight",
          lastName:"Wood",
          email:"dwight.wood38@example.com",
          phone:"(890)-778-9023",
          location: {
            city:"North Valley",
            state:"Ohio",
            street:"3677 Walnut Hill lLn",
            zip:"12967"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/82.jpg",
            "http://api.randomuser.me/portraits/med/men/82.jpg",
            "http://api.randomuser.me/portraits/thumb/men/82.jpg"
          ]
        },{
          id:25,
          firstName:"Roland",
          lastName:"Thomas",
          email:"roland.thomas46@example.com",
          phone:"(711)-326-9419",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"6735 N Stelling Rd",
            zip:"91200"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/38.jpg",
            "http://api.randomuser.me/portraits/med/men/38.jpg",
            "http://api.randomuser.me/portraits/thumb/men/38.jpg"
          ]
        }, {
          id:26,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:27,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        }
      ]
    },{
      id:0,
      eventTitle:"Photo Club: Grand Canyon Trip",
      timestamp:1431162000,
      eventDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor pretium ligulain facilisis lacus fringilla elementum. Cras sit amet luctus urna, lobortis convallis lectus. Nullam augue ipsum, suscipit ac gravida ac, pellentesque eu justo. Etiam tempor.",
      eventImage:"http://images.boomsbeat.com/data/images/full/653/26-jpg.jpg",
      attendees:[
        {
          id:2,
          firstName:"Denise",
          lastName:"Rose",
          email:"denise.rose54@example.com",
          phone:"(296)-436-6580",
          location: {
            city:"Bueblo",
            state:"Maine",
            street:"3994 Spring St",
            zip:"49751"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/57.jpg",
            "http://api.randomuser.me/portraits/med/women/57.jpg",
            "http://api.randomuser.me/portraits/thumb/women/57.jpg"
          ]
        },{
          id:3,
          firstName:"Dwight",
          lastName:"Wood",
          email:"dwight.wood38@example.com",
          phone:"(890)-778-9023",
          location: {
            city:"North Valley",
            state:"Ohio",
            street:"3677 Walnut Hill lLn",
            zip:"12967"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/82.jpg",
            "http://api.randomuser.me/portraits/med/men/82.jpg",
            "http://api.randomuser.me/portraits/thumb/men/82.jpg"
          ]
        },{
          id:4,
          firstName:"Roland",
          lastName:"Thomas",
          email:"roland.thomas46@example.com",
          phone:"(711)-326-9419",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"6735 N Stelling Rd",
            zip:"91200"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/38.jpg",
            "http://api.randomuser.me/portraits/med/men/38.jpg",
            "http://api.randomuser.me/portraits/thumb/men/38.jpg"
          ]
        },{
          id:22,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        }
      ]
    },{
      id:0,
      eventTitle:"Tough Mudder, Salt Lake City",
      timestamp:1431162000,
      eventDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor pretium ligulain facilisis lacus fringilla elementum. Cras sit amet luctus urna, lobortis convallis lectus. Nullam augue ipsum, suscipit ac gravida ac, pellentesque eu justo. Etiam tempor.",
      eventImage:"http://positiveworldtravel.smugmug.com/Australia/Tough-Mudder-2012/i-fcwqtJS/0/L/Tough.Mudder-47-L.jpg",
      attendees:[
        {
          id:0,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:1,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        },{
          id:2,
          firstName:"Denise",
          lastName:"Rose",
          email:"denise.rose54@example.com",
          phone:"(296)-436-6580",
          location: {
            city:"Bueblo",
            state:"Maine",
            street:"3994 Spring St",
            zip:"49751"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/57.jpg",
            "http://api.randomuser.me/portraits/med/women/57.jpg",
            "http://api.randomuser.me/portraits/thumb/women/57.jpg"
          ]
        },{
          id:3,
          firstName:"Dwight",
          lastName:"Wood",
          email:"dwight.wood38@example.com",
          phone:"(890)-778-9023",
          location: {
            city:"North Valley",
            state:"Ohio",
            street:"3677 Walnut Hill lLn",
            zip:"12967"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/82.jpg",
            "http://api.randomuser.me/portraits/med/men/82.jpg",
            "http://api.randomuser.me/portraits/thumb/men/82.jpg"
          ]
        },{
          id:4,
          firstName:"Roland",
          lastName:"Thomas",
          email:"roland.thomas46@example.com",
          phone:"(711)-326-9419",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"6735 N Stelling Rd",
            zip:"91200"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/38.jpg",
            "http://api.randomuser.me/portraits/med/men/38.jpg",
            "http://api.randomuser.me/portraits/thumb/men/38.jpg"
          ]
        }, {
          id:5,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:6,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        },{
          id:7,
          firstName:"Rene",
          lastName:"Davidson",
          email:"rene.davidson68@example.com",
          phone:"(957)-633-1372",
          location: {
            city:"Sacramento",
            state:"Oklahoma",
            street:"5387 Miller ave",
            zip:"60488"
          },
          photos:[
            "http://api.randomuser.me/portraits/men/25.jpg",
            "http://api.randomuser.me/portraits/med/men/25.jpg",
            "http://api.randomuser.me/portraits/thumb/men/25.jpg"
          ]
        },{
          id:8,
          firstName:"Edith",
          lastName:"Daniel",
          email:"edith.daniels15@example.com",
          phone:"(715)-357-1848",
          location: {
            city:"Las Vegas",
            state:"Nevada",
            street:"9010 E Center St",
            zip:"71489"
          },
          photos:[
            "http://api.randomuser.me/portraits/women/19.jpg",
            "http://api.randomuser.me/portraits/med/women/19.jpg",
            "http://api.randomuser.me/portraits/thumb/women/19.jpg"
          ]
        }
      ]
    }
  ];


  // $scope.streamData = $scope.shuffle($scope.userData.concat($scope.restaurantData));
  $scope.streamData = $scope.userData;
  
  });

