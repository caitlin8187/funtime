angular.module('ngSoloists.yelpService', [])

.factory('YelpService', ['$http', function($http) {

  var getRestaurants = function(success, error, data) {
    var req = $http.get("http://localhost:1337/?term="+data.cuisine+"&location="+data.zip+"&offset="+data.page+"&limit="+data.perPage)
    .success(success)
    .error(error);

    return req;
  };




  return {
    getRestaurants:getRestaurants
  };

}]);


