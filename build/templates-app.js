angular.module('templates-app', ['about/about.tpl.html', 'directives/filter.tpl.html', 'directives/peopleList.tpl.html', 'directives/profile.tpl.html', 'directives/stream.tpl.html', 'home/home.tpl.html', 'partials/event.tpl.html']);

angular.module("about/about.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("about/about.tpl.html",
    "<div class=\"row\">\n" +
    "  <h1 class=\"page-header\">\n" +
    "    The Elevator Pitch\n" +
    "    <small>For the lazy and impatient.</small>\n" +
    "  </h1>\n" +
    "  <p>\n" +
    "    <code>ng-boilerplate</code> is an opinionated kickstarter for web\n" +
    "    development projects. It's an attempt to create a simple starter for new\n" +
    "    web sites and apps: just download it and start coding. The goal is to\n" +
    "    have everything you need to get started out of the box; of course it has\n" +
    "    slick styles and icons, but it also has a best practice directory structure\n" +
    "    to ensure maximum code reuse. And it's all tied together with a robust\n" +
    "    build system chock-full of some time-saving goodness.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>Why?</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Because my team and I make web apps, and \n" +
    "    last year AngularJS became our client-side framework of choice. We start\n" +
    "    websites the same way every time: create a directory structure, copy and\n" +
    "    ever-so-slightly tweak some config files from an older project, and yada\n" +
    "    yada, etc., and so on and so forth. Why are we repeating ourselves? We wanted a starting point; a set of\n" +
    "    best practices that we could identify our projects as embodying and a set of\n" +
    "    time-saving wonderfulness, because time is money.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    There are other similar projects out there, but none of them suited our\n" +
    "    needs. Some are awesome but were just too much, existing more as reference\n" +
    "    implementations, when we really just wanted a kickstarter. Others were just\n" +
    "    too little, with puny build systems and unscalable architectures.  So we\n" +
    "    designed <code>ng-boilerplate</code> to be just right.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>What ng-boilerplate Is Not</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    This is not an example of an AngularJS app. This is a kickstarter. If\n" +
    "    you're looking for an example of what a complete, non-trivial AngularJS app\n" +
    "    that does something real looks like, complete with a REST backend and\n" +
    "    authentication and authorization, then take a look at <code><a\n" +
    "        href=\"https://github.com/angular-app/angular-app/\">angular-app</a></code>, \n" +
    "    which does just that, and does it well.\n" +
    "  </p>\n" +
    "\n" +
    "  <h1 class=\"page-header\">\n" +
    "    So What's Included?\n" +
    "    <small>I'll try to be more specific than \"awesomeness\".</small>\n" +
    "  </h1>\n" +
    "  <p>\n" +
    "    This section is just a quick introduction to all the junk that comes\n" +
    "    pre-packaged with <code>ng-boilerplate</code>. For information on how to\n" +
    "    use it, see the <a\n" +
    "      href=\"https://github.com/joshdmiller/ng-boilerplate#readme\">project page</a> at\n" +
    "    GitHub.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    The high-altitude view is that the base project includes \n" +
    "    <a href=\"http://getbootstrap.com\">Twitter Bootstrap</a>\n" +
    "    styles to quickly produce slick-looking responsive web sites and apps. It also\n" +
    "    includes <a href=\"http://angular-ui.github.com/bootstrap\">UI Bootstrap</a>,\n" +
    "    a collection of native AngularJS directives based on the aforementioned\n" +
    "    templates and styles. It also includes <a href=\"http://fortawesome.github.com/Font-Awesome/\">Font Awesome</a>,\n" +
    "    a wicked-cool collection of font-based icons that work swimmingly with all\n" +
    "    manner of web projects; in fact, all images on the site are actually font-\n" +
    "    based icons from Font Awesome. Neat! Lastly, this also includes\n" +
    "    <a href=\"http://joshdmiller.github.com/angular-placeholders\">Angular Placeholders</a>,\n" +
    "    a set of pure AngularJS directives to do client-side placeholder images and\n" +
    "    text to make mocking user interfaces super easy.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    And, of course, <code>ng-boilerplate</code> is built on <a href=\"http://angularjs.org\">AngularJS</a>,\n" +
    "    by the far the best JavaScript framework out there! But if you don't know\n" +
    "    that already, then how did you get here? Well, no matter - just drink the\n" +
    "    Kool Aid. Do it. You know you want to.\n" +
    "  </p>\n" +
    "  \n" +
    "  <h2>Twitter Bootstrap</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    You already know about this, right? If not, <a\n" +
    "      href=\"http://getbootstrap.com\">hop on over</a> and check it out; it's\n" +
    "    pretty sweet. Anyway, all that wonderful stylistic goodness comes built in.\n" +
    "    The LESS files are available for you to import in your main stylesheet as\n" +
    "    needed - no excess, no waste. There is also a dedicated place to override\n" +
    "    variables and mixins to suit your specific needs, so updating to the latest\n" +
    "    version of Bootstrap is as simple as: \n" +
    "  </p>\n" +
    "\n" +
    "  <pre>$ cd vendor/twitter-bootstrap<br />$ git pull origin master</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    Boom! And victory is ours.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>UI Bootstrap</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    What's better than Bootstrap styles? Bootstrap directives!  The fantastic <a href=\"http://angular-ui.github.com/bootstrap\">UI Bootstrap</a>\n" +
    "    library contains a set of native AngularJS directives that are endlessly\n" +
    "    extensible. You get the tabs, the tooltips, the accordions. You get your\n" +
    "    carousel, your modals, your pagination. And <i>more</i>.\n" +
    "    How about a quick demo? \n" +
    "  </p>\n" +
    "\n" +
    "  <ul>\n" +
    "    <li class=\"dropdown\">\n" +
    "      <a class=\"btn dropdown-toggle\">\n" +
    "        Click me!\n" +
    "      </a>\n" +
    "      <ul class=\"dropdown-menu\">\n" +
    "        <li ng-repeat=\"choice in dropdownDemoItems\">\n" +
    "          <a>{{choice}}</a>\n" +
    "        </li>\n" +
    "      </ul>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "\n" +
    "  <p>\n" +
    "    Oh, and don't include jQuery;  \n" +
    "    you don't need it.\n" +
    "    This is better.\n" +
    "    Don't be one of those people. <sup>*</sup>\n" +
    "  </p>\n" +
    "\n" +
    "  <p><small><sup>*</sup> Yes, there are exceptions.</small></p>\n" +
    "\n" +
    "  <h2>Font Awesome</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Font Awesome has earned its label. It's a set of open (!) icons that come\n" +
    "    distributed as a font (!) for super-easy, lightweight use. Font Awesome \n" +
    "    works really well with Twitter Bootstrap, and so fits right in here.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    There is not a single image on this site. All of the little images and icons \n" +
    "    are drawn through Font Awesome! All it takes is a little class:\n" +
    "  </p>\n" +
    "\n" +
    "  <pre>&lt;i class=\"fa fa-flag\"&gt;&lt/i&gt</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    And you get one of these: <i class=\"fa fa-flag\"> </i>. Neat!\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>Placeholders</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Angular Placeholders is a simple library for mocking up text and images. You\n" +
    "    can automatically throw around some \"lorem ipsum\" text:\n" +
    "  </p>\n" +
    "\n" +
    "  <pre>&lt;p ph-txt=\"3s\"&gt;&lt;/p&gt;</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    Which gives you this:\n" +
    "  </p>\n" +
    "\n" +
    "  <div class=\"pre\">\n" +
    "    &lt;p&gt;\n" +
    "    <p ph-txt=\"3s\"></p>\n" +
    "    &lt;/p&gt;\n" +
    "  </div>\n" +
    "\n" +
    "  <p>\n" +
    "    Even more exciting, you can also create placeholder images, entirely \n" +
    "    client-side! For example, this:\n" +
    "  </p>\n" +
    "  \n" +
    "  <pre>\n" +
    "&lt;img ph-img=\"50x50\" /&gt;\n" +
    "&lt;img ph-img=\"50x50\" class=\"img-polaroid\" /&gt;\n" +
    "&lt;img ph-img=\"50x50\" class=\"img-rounded\" /&gt;\n" +
    "&lt;img ph-img=\"50x50\" class=\"img-circle\" /&gt;</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    Gives you this:\n" +
    "  </p>\n" +
    "\n" +
    "  <div>\n" +
    "    <img ph-img=\"50x50\" />\n" +
    "    <img ph-img=\"50x50\" class=\"img-polaroid\" />\n" +
    "    <img ph-img=\"50x50\" class=\"img-rounded\" />\n" +
    "    <img ph-img=\"50x50\" class=\"img-circle\" />\n" +
    "  </div>\n" +
    "\n" +
    "  <p>\n" +
    "    Which is awesome.\n" +
    "  </p>\n" +
    "\n" +
    "  <h1 class=\"page-header\">\n" +
    "    The Roadmap\n" +
    "    <small>Really? What more could you want?</small>\n" +
    "  </h1>\n" +
    "\n" +
    "  <p>\n" +
    "    This is a project that is <i>not</i> broad in scope, so there's not really\n" +
    "    much of a wish list here. But I would like to see a couple of things:\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    I'd like it to be a little simpler. I want this to be a universal starting\n" +
    "    point. If someone is starting a new AngularJS project, she should be able to\n" +
    "    clone, merge, or download its source and immediately start doing what she\n" +
    "    needs without renaming a bunch of files and methods or deleting spare parts\n" +
    "    ... like this page. This works for a first release, but I just think there\n" +
    "    is a little too much here right now.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    I'd also like to see a simple generator. Nothing like <a href=\"yeoman.io\">\n" +
    "    Yeoman</a>, as there already is one of those, but just something that\n" +
    "    says \"I want Bootstrap but not Font Awesome and my app is called 'coolApp'.\n" +
    "    Gimme.\" Perhaps a custom download builder like UI Bootstrap\n" +
    "    has. Like that. Then again, perhaps some Yeoman generators wouldn't be out\n" +
    "    of line. I don't know. What do you think?\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    Naturally, I am open to all manner of ideas and suggestions. See the \"Can I\n" +
    "    Help?\" section below.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>The Tactical To Do List</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    There isn't much of a demonstration of UI Bootstrap. I don't want to pollute\n" +
    "    the code with a demo for demo's sake, but I feel we should showcase it in\n" +
    "    <i>some</i> way.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    <code>ng-boilerplate</code> should include end-to-end tests. This should\n" +
    "    happen soon. I just haven't had the time.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    Lastly, this site should be prettier, but I'm no web designer. Throw me a\n" +
    "    bone here, people!\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>Can I Help?</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Yes, please!\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    This is an opinionated kickstarter, but the opinions are fluid and\n" +
    "    evidence-based. Don't like the way I did something? Think you know of a\n" +
    "    better way? Have an idea to make this more useful? Let me know! You can\n" +
    "    contact me through all the usual channels or you can open an issue on the\n" +
    "    GitHub page. If you're feeling ambitious, you can even submit a pull\n" +
    "    request - how thoughtful of you!\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    So join the team! We're good people.\n" +
    "  </p>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("directives/filter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/filter.tpl.html",
    "<div class=\"navbar navbar-default\" ng-click=\"expandFilter()\">\n" +
    "  <div class=\"navbar-header\">\n" +
    "    <button type=\"button\" class=\"navbar-toggle\" ng-init=\"menuCollapsed = true\"\n" +
    "    ng-click=\"menuCollapsed = ! menuCollapsed\">\n" +
    "    <span class=\"sr-only\">Toggle navigation</span>\n" +
    "    <span class=\"icon-bar\"></span>\n" +
    "    <span class=\"icon-bar\"></span>\n" +
    "    <span class=\"icon-bar\"></span>\n" +
    "  </button>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"navbar-brand\"></div>\n" +
    "<div class=\"btn_settings\"></div>\n" +
    "\n" +
    "<div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "\n" +
    "  <form class=\"margin-top-medium margin-bottom-medium\">\n" +
    "    <div class=\"single-input filter-search\">\n" +
    "      <span class=\"txt\">What do you like?</span>\n" +
    "      <input type=\"text\" id=\"filter-search\" ng-change=\"changingFilter()\">\n" +
    "      <button type=\"submit\">Add</button>\n" +
    "      <div class=\"clearfix\"></div>\n" +
    "    </div>\n" +
    "  </form>\n" +
    "\n" +
    "  <form class=\"navbar-form\" role=\"search\">\n" +
    "    <p class=\"title-text\">Your Interests:</p>\n" +
    "    <div class=\"form-group\">\n" +
    "      <div class=\"single-input filter-item\">\n" +
    "        <input type=\"checkbox\" ng-model=\"filterOptions.crossfit\" id=\"filter-crossfit\" ng-change=\"changingFilter()\">\n" +
    "        <label for=\"filter-crossfit\">\n" +
    "          <span class=\"txt\">Cross Fit</span>\n" +
    "          <span class=\"btn_close\">X</span>\n" +
    "          <div></div>\n" +
    "        </label>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"single-input filter-item\">\n" +
    "        <input type=\"checkbox\" ng-model=\"filterOptions.mountainbiking\" id=\"filter-mountainbiking\" ng-change=\"changingFilter()\">\n" +
    "        <label for=\"filter-mountainbiking\">\n" +
    "          <span class=\"txt\">Mountain Biking</span>\n" +
    "          <span class=\"btn_close\">X</span>\n" +
    "          <div></div>\n" +
    "        </label>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"single-input filter-item\">\n" +
    "        <input type=\"checkbox\" ng-model=\"filterOptions.movies\" id=\"filter-movies\" ng-change=\"changingFilter()\">\n" +
    "        <label for=\"filter-movies\">\n" +
    "          <span class=\"txt\">Movies</span>\n" +
    "          <span class=\"btn_close\">X</span>\n" +
    "          <div></div>\n" +
    "        </label>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"single-input filter-item\">\n" +
    "        <input type=\"checkbox\" ng-model=\"filterOptions.photography\" id=\"filter-photography\" ng-change=\"changingFilter()\">\n" +
    "        <label for=\"filter-photography\">\n" +
    "          <span class=\"txt\">Photography</span>\n" +
    "          <span class=\"btn_close\">X</span>\n" +
    "          <div></div>\n" +
    "        </label>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"single-input filter-item\">\n" +
    "        <input type=\"checkbox\" ng-model=\"filterOptions.soccer\" id=\"filter-soccer\" ng-change=\"changingFilter()\">\n" +
    "        <label for=\"filter-soccer\">\n" +
    "          <span class=\"txt\">Soccer</span>\n" +
    "          <span class=\"btn_close\">X</span>\n" +
    "          <div></div>\n" +
    "        </label>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"single-input filter-item\">\n" +
    "        <input type=\"checkbox\" ng-model=\"filterOptions.trailrunning\" id=\"filter-trailrunning\" ng-change=\"changingFilter()\">\n" +
    "        <label for=\"filter-trailrunning\">\n" +
    "          <span class=\"txt\">Trail Running</span>\n" +
    "          <span class=\"btn_close\">X</span>\n" +
    "          <div></div>\n" +
    "        </label>\n" +
    "      </div>\n" +
    "\n" +
    "      \n" +
    "    </div>\n" +
    "  </form>\n" +
    "</div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("directives/peopleList.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/peopleList.tpl.html",
    "<div class=\"people-list\">\n" +
    "	<p class=\"title-text\">People Who Like <br/>What You Like</p>\n" +
    "	<div class=\"people-container\">\n" +
    "        <div ng-repeat=\"person in people\" class=\"person\">\n" +
    "			<div class=\"img-crop\" ng-hide=\"$index >= 5\">\n" +
    "				<img src=\"{{person.photos[2]}}\"/>\n" +
    "			</div>\n" +
    "        </div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("directives/profile.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/profile.tpl.html",
    "<div class=\"profile-container\">\n" +
    "  <div class=\"event\" ng-show=\"selectedItem!==undefined\">\n" +
    "\n" +
    "    <div class=\"header-img\">\n" +
    "      <p ng-if=\"selectedItem.firstName\">{{(selectedItem.firstName + \" \" + selectedItem.lastName.substr(0, 1))}}. <br/>\n" +
    "      <i>{{selectedItem.location.city}}, {{selectedItem.location.state}}</i></p>\n" +
    "      \n" +
    "      <p ng-if=\"selectedItem.restaurantName\">{{selectedItem.restaurantName}}<br/>\n" +
    "      <i>{{selectedItem.cuisine}}</i></p>\n" +
    "      \n" +
    "      <img src=\"{{selectedItem.headerImg}}\">\n" +
    "      <div class=\"img-crop\">\n" +
    "        <img src=\"{{selectedItem.logo || selectedItem.photos[1]}}\"/>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "    <div class=\"tab-container pull-left margin-top-small\">\n" +
    "      <tabset>\n" +
    "        <tab heading=\"List View\">\n" +
    "\n" +
    "          <div class=\"selected-event-container\" ng-show=\"selectedEvents.length > 0\">\n" +
    "            <div ng-repeat=\"event in selectedEvents\" class=\"selected-event\">\n" +
    "              <div class=\"btn-close\" ng-click=\"removeSelectedEvent($index)\"><i class=\"fa fa-times\"></i></div>\n" +
    "              \n" +
    "              <div class=\"content col-md-9 padding-none\">\n" +
    "                <div class=\"logo-restaurant\">\n" +
    "                  <img src=\"{{event.logo}}\" sol-center-img container-width=\"160\" container-height=\"130\"/>\n" +
    "                </div>\n" +
    "                <div class=\"details\">\n" +
    "                  <h2>{{event.title}}</h2>\n" +
    "                  <span class=\"description-cuisine\">{{event.description}}</span>\n" +
    "                  <span class=\"description-timedate\">{{event.start}}</span>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "                \n" +
    "              <div class=\"col-md-3 buttons\">\n" +
    "                <button class=\"btn btn-default rsvp\" ng-class=\"{red: selectedItem.status.soloistsEateries==='soloists' && selectedItem.status.travelersLocals==='locals', yellow: selectedItem.status.soloistsEateries==='soloists' && selectedItem.status.travelersLocals==='travelers' && selectedItem.status.businessLeisure==='business', green: selectedItem.status.soloistsEateries==='soloists' && selectedItem.status.travelersLocals==='travelers' && selectedItem.status.businessLeisure==='leisure'}\">SAVE</button>\n" +
    "                <button class=\"btn btn-default\">Map It</button>\n" +
    "                <button class=\"btn btn-default\">Info&nbsp;<i class=\"fa fa-caret-right\"></i></button>\n" +
    "              </div>\n" +
    "\n" +
    "              <div class=\"clearfix\"></div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"calendar\" ng-if=\"selectedItem\" ng-model=\"eventSources\" calendar=\"profileCalendar\" config=\"uiConfig.calendar\" ui-calendar=\"uiConfig.calendar\"></div>\n" +
    "\n" +
    "\n" +
    "          <!-- <ul class=\"restaurant-list\">\n" +
    "            <li>\n" +
    "              <div class=\"img-crop\">\n" +
    "                <img src=\"http://www.miami.com/sites/migration.miami.com/files/images/bulla-restaurant-logo.png\"/>\n" +
    "              </div>\n" +
    "\n" +
    "              <div class=\"info\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                  <h2 class=\"title\">Restaurant Name</h2>\n" +
    "                  <p class=\"description\">Mediterranean, Greek, Gyro, Fallafel, Rice Pudding</p>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                  <button type=\"button\" class=\"btn btn-default btn-lg\">\n" +
    "                    <span class=\"fa fa-plus-square\" aria-hidden=\"true\"></span>&nbsp;&nbsp;Add\n" +
    "                  </button>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </li>\n" +
    "          </ul> -->\n" +
    "        </tab>\n" +
    "        \n" +
    "\n" +
    "\n" +
    "        <tab heading=\"Map View\" ng-click=\"refreshMap(36.119454, -115.149594)\">\n" +
    "          <ui-gmap-google-map events=\"map.events\" draggable=\"true\" center=\"map.center\" zoom=\"map.zoom\" control=\"control\">\n" +
    "            <ui-gmap-markers models=\"selectedItem.events\" coords=\"'coords'\" options=\"'options'\" events=\"'markerEvents'\"></ui-gmap-markers>\n" +
    "          </ui-gmap-google-map>\n" +
    "        </tab>\n" +
    "\n" +
    "        \n" +
    "\n" +
    "        <tab class=\"pull-right last\" heading=\"Chat\"></tab>\n" +
    "        \n" +
    "      </tabset>\n" +
    "    </div>\n" +
    "    <div class=\"clearfix padding-bottom-small\"></div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("directives/stream.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/stream.tpl.html",
    "<div class=\"stream-container\">\n" +
    "\n" +
    "\n" +
    "  <div infinite-scroll='loadMore()' infinite-scroll-distance='2'>\n" +
    "    <div ng-repeat=\"item in images track by $index\" class=\"stream-item\" ng-class=\"{yellow: $index%3===0, green: ($index+1)%3===0, red: ($index+2)%3===0}\" style=\"background-image:url('{{item.eventImage}}')\">\n" +
    "      <div class=\"soloist\">\n" +
    "        <div class=\"txt\">\n" +
    "          <h3 class=\"headline\">{{item.eventTitle}}</h3>\n" +
    "          <p class=\"datetime\">{{convertTime(item.timestamp)}}</p>\n" +
    "          <p class=\"description\">\n" +
    "            <span ng-class=\"{'description-full': showDescription}\">{{item.eventDescription}}<span class=\"btn-less\" ng-click=\"showDescription = false\" ng-show=\"showDescription\">(less)</span></span>\n" +
    "            <span class=\"btn-more\" ng-click=\"showDescription = true\" ng-hide=\"showDescription\">(more)</span>\n" +
    "          </p>\n" +
    "          <div class=\"clearfix\"></div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"people-container\">\n" +
    "          <div ng-repeat=\"attendee in item.attendees | amountToShow:minCount\" class=\"person\">\n" +
    "            <div class=\"img-crop\" >\n" +
    "              <img src=\"{{attendee.photos[2]}}\"/>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <span class=\"btn-more\" ng-hide=\"minCount>=item.attendees.length\" ng-click=\"minCount=item.attendees.length\">+{{item.attendees.length-5}} more</span>\n" +
    "          <div class=\"clearfix\"></div>\n" +
    "        </div>\n" +
    "\n" +
    "        <button>I'm Interested</button>\n" +
    "        <div class=\"clearfix\"></div>\n" +
    "      </div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "      \n" +
    "      <div class=\"clearfix\"></div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "\n" +
    "\n" +
    "  \n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<div sol-filter expand-or-collapse=\"expandOrCollapse\" filter-updated=\"filterUpdated\" filter-options=\"filterOptions\" class=\"directive-filter col-md-3\"></div>\n" +
    "<div class=\"content-container col-md-7 padding-none\">\n" +
    "	<div sol-stream expand-or-collapse=\"expandOrCollapse\" filter-updated=\"filterUpdated\" filter-options=\"filterOptions\" class=\"directive-stream\" data=\"streamData\" selected-item=\"selectedItem\"></div>\n" +
    "	<div sol-profile ng-show=\"expandOrCollapse==='collapsed'\" class=\"directive-profile\" data=\"profileData\" selected-item=\"selectedItem\"></div>\n" +
    "</div>\n" +
    "<div sol-people-list class=\"directive-people-list col-md-2\"></div>\n" +
    "\n" +
    "");
}]);

angular.module("partials/event.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/event.tpl.html",
    "<div sol-filter class=\"directive-filter col-md-3\"></div>\n" +
    "<div class=\"col-md-3\"></div>\n" +
    "<div sol-stream class=\"directive-stream col-md-9\" data=streamData></div>\n" +
    "");
}]);
