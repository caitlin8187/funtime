angular.module('ngSoloists.peopleList', [])

.directive("solPeopleList", function() {
	return {
		restrict: "A",
		templateUrl: 'directives/peopleList.tpl.html',
		link: function(scope, element, attrs) {
			
			scope.people = [
			{
				id:0,
				firstName:"Rene",
				lastName:"Davidson",
				email:"rene.davidson68@example.com",
				phone:"(957)-633-1372",
				location: {
					city:"Sacramento",
					state:"Oklahoma",
					street:"5387 Miller ave",
					zip:"60488"
				},
				photos:[
				"http://api.randomuser.me/portraits/men/25.jpg",
				"http://api.randomuser.me/portraits/med/men/25.jpg",
				"http://api.randomuser.me/portraits/thumb/men/25.jpg"
				]
			},{
				id:1,
				firstName:"Edith",
				lastName:"Daniel",
				email:"edith.daniels15@example.com",
				phone:"(715)-357-1848",
				location: {
					city:"Las Vegas",
					state:"Nevada",
					street:"9010 E Center St",
					zip:"71489"
				},
				photos:[
				"http://api.randomuser.me/portraits/women/19.jpg",
				"http://api.randomuser.me/portraits/med/women/19.jpg",
				"http://api.randomuser.me/portraits/thumb/women/19.jpg"
				]
			},{
				id:2,
				firstName:"Denise",
				lastName:"Rose",
				email:"denise.rose54@example.com",
				phone:"(296)-436-6580",
				location: {
					city:"Bueblo",
					state:"Maine",
					street:"3994 Spring St",
					zip:"49751"
				},
				photos:[
				"http://api.randomuser.me/portraits/women/57.jpg",
				"http://api.randomuser.me/portraits/med/women/57.jpg",
				"http://api.randomuser.me/portraits/thumb/women/57.jpg"
				]
			},{
				id:3,
				firstName:"Dwight",
				lastName:"Wood",
				email:"dwight.wood38@example.com",
				phone:"(890)-778-9023",
				location: {
					city:"North Valley",
					state:"Ohio",
					street:"3677 Walnut Hill lLn",
					zip:"12967"
				},
				photos:[
				"http://api.randomuser.me/portraits/men/82.jpg",
				"http://api.randomuser.me/portraits/med/men/82.jpg",
				"http://api.randomuser.me/portraits/thumb/men/82.jpg"
				]
			},{
				id:4,
				firstName:"Roland",
				lastName:"Thomas",
				email:"roland.thomas46@example.com",
				phone:"(711)-326-9419",
				location: {
					city:"Las Vegas",
					state:"Nevada",
					street:"6735 N Stelling Rd",
					zip:"91200"
				},
				photos:[
				"http://api.randomuser.me/portraits/men/38.jpg",
				"http://api.randomuser.me/portraits/med/men/38.jpg",
				"http://api.randomuser.me/portraits/thumb/men/38.jpg"
				]
			}, {
				id:5,
				firstName:"Rene",
				lastName:"Davidson",
				email:"rene.davidson68@example.com",
				phone:"(957)-633-1372",
				location: {
					city:"Sacramento",
					state:"Oklahoma",
					street:"5387 Miller ave",
					zip:"60488"
				},
				photos:[
				"http://api.randomuser.me/portraits/men/25.jpg",
				"http://api.randomuser.me/portraits/med/men/25.jpg",
				"http://api.randomuser.me/portraits/thumb/men/25.jpg"
				]
			},{
				id:6,
				firstName:"Edith",
				lastName:"Daniel",
				email:"edith.daniels15@example.com",
				phone:"(715)-357-1848",
				location: {
					city:"Las Vegas",
					state:"Nevada",
					street:"9010 E Center St",
					zip:"71489"
				},
				photos:[
				"http://api.randomuser.me/portraits/women/19.jpg",
				"http://api.randomuser.me/portraits/med/women/19.jpg",
				"http://api.randomuser.me/portraits/thumb/women/19.jpg"
				]
			}
			];
		}
	};
});

