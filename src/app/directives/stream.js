angular.module('ngSoloists.stream', ['infinite-scroll'])

.directive("solStream", function($window, $http) {
	return {
		scope: {
			data:"=",
			expandOrCollapse:"=",
			filterOptions:"=",
			filterUpdated:"=",
			selectedItem:"="
		},
		restrict: "A",
		templateUrl: 'directives/stream.tpl.html',
		link: function(scope, element, attrs) {
			// console.log(scope.data);

			scope.showDescription = false;
			scope.minCount = 5;

			scope.convertTime = function(timestamp) {
				return moment(timestamp).format('MMMM D, h:mma');
			};

			var lastScrollTop = 0;

			angular.element($window).bind("scroll", function() {
				var scrollTop = $(window).scrollTop();
				var diff = 30;
				if ($('.directive-profile').height() > $(window).height()) {
					diff = ($('.directive-profile').height() - $(window).height());

					var profileTop = parseInt($('.directive-profile').css('margin-top'), 10);
					if (scrollTop < profileTop) {
						$('.directive-profile').css('margin-top', scrollTop);
					} else if ((scrollTop - diff) > profileTop) {
						$('.directive-profile').css('margin-top', (scrollTop - diff));
					}

					if (scrollTop < 0) {
						$('.directive-profile').css('margin-top', 0);
					}
				} else {
					$('.directive-profile').css('margin-top', scrollTop);
				}
			});

			angular.element($window).bind("scrollstart", function() {
				lastScrollTop = $(window).scrollTop();
			});


			
			scope.expandItem = function(e, item, index) {
				var targ = $(e.target);

				$('.expanded').removeClass('expanded');
				$(targ).addClass('expanded') ;

				scope.collapseFilter();
				scope.selectedItem = item;
				scope.selectedItem.index = index;
			};


			scope.collapseFilter = function() {
				$('.directive-filter').addClass('collapsed');
				$('.directive-profile').addClass('collapsed');
				$('.content-container').addClass('collapsed col-md-12').removeClass('col-md-9');

				scope.expandOrCollapse = 'collapsed';
			};


			scope.getOriginalImage = function(url) {
				// http://s3-media1.fl.yelpcdn.com/bphoto/n5jyZXaK8OL1oRTsszIe0A/o.jpg
				var urlSubstr = url.substr((url.length-6), url.length);
				var newUrl = url.replace(urlSubstr, 'o.jpg');
				
				return newUrl;
			};

			scope.images = scope.data;
			scope.ogLength = scope.data.length;

			// Need server-side pagination on loaded data.
			// Pulling all dummy data in at the moment.
			scope.loadMore = function() {
				for(var i = 0; i<scope.ogLength; i++) {
					var last = scope.images[i];
					scope.images.push(last);
				}
			};
		}
	};
})




.directive('solCenterImg', function () {       
	return {
		scope: {
			containerWidth:"=",
			containerHeight:"="
		},
		restrict: "A",
		link: function(scope, element, attrs) {
			element.bind('load', function() {
				if (element.width() >= scope.containerWidth) {
					element.css({
						'width':'100%',
						'height':'auto'
					});
				}

				if (element.height() >= scope.containerHeight) {
					element.css({
						'height':'100%',
						'width':'auto'
					});
				}

				element.css({
					// 'left':(scope.containerWidth/2 - element.width()/2),
					'top':(scope.containerHeight/2 - element.height()/2)
				});
			});
		}
	};
})




.filter('amountToShow', function() {
	return function(items, minCount) {

		var filteredItems = [];

		angular.forEach(items, function(item) {
			if (item.id < minCount) {
				filteredItems.push(item);
			}
		});

		return filteredItems;
	};
});




