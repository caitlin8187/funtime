angular.module( 'ngSoloists', [
  'ui.utils',
  'ui.select',
  'ui.router',
  'ui.bootstrap',
  'templates-app',
  'templates-common',
  'ngSoloists.home',
  'ngSoloists.about',
  'ngSoloists.filter',
  'ngSoloists.peopleList',
  'ngSoloists.stream',
  'ngSoloists.profile',
  'ngSoloists.userService',
  'ngSoloists.yelpService',
  'ui.calendar',
  'uiGmapgoogle-maps'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/home' );
})

.run( function run () {

})

.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyA44w5HOzQieu-53ldcOCe1XT28xkPXSXw',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | ngSoloists' ;
    }
  });
});

