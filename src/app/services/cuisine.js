angular.module('ngSoloists.CuisineService', [
	'ngSoloists.CuisineService'
])

.factory('CuisineService', ['$http', function($http) {


  var getCuisines = function(success, error) {
    var req = $http.get("assets/mocks/cuisine.json")
    .success(success)
    .error(error);

    return req;
  };



  return {
    getCuisines:getCuisines
  };

}]);


